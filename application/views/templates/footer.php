<script type="text/javascript" src="<?= base_url('assets/assets/js/app.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/assets/node_modules/mdbootstrap/js/mdb.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/assets/node_modules/mdbootstrap/js/modules/forms-free.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/assets/js/agile-min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/assets/js/jquery.validate.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/assets/js/jquery-confirm.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.blockUI.js') ?>"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('input.form-control').on('focus', function () {
            $(this).toggleClass('active');
        });
    });
</script>