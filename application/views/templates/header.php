<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>RODEO CARWASH</title>

<link rel="shortcut icon" type="image/png" href="<?= base_url('assets/logo.png') ?>"/>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/assets/node_modules/mdbootstrap/css/mdb.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/assets/node_modules/mdbootstrap/css/modules/animations-extended.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/assets/css/jquery-confirm.min.css?new') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/assets/css/compiled-4.8.8.min.css?new1') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/assets/css/style.css?new4') ?>">