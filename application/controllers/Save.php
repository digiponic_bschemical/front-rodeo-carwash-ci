<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\CapabilityProfile;

class Save extends CI_Controller {

  // var $baseurl = 'http://localhost/rodeo-car-wash/public/api';

  public function __construct(){
    parent::__construct();
    $this->load->model('M_data');
    $this->load->helper('curl_helper');
  }
  
  public function index(){
    $services = json_decode(stripslashes($this->input->post('services')));
    $kendaraan = $this->input->post('id_kendaraan');
    $data = array('id_cabang' => 1, 'id_kendaraan' => $kendaraan, 'services' => $services);
    $response = json_decode(curl($this->config->item('base_url_api') . '/reservasi/ots', $data), true);
    if ($response['msg'] == 'Reservasi Berhasil'){
      $resp = curl($this->config->item('base_url_api') . '/reservasi/detail/' . $response['data']);
      $result = json_decode($resp);
      if ($result){
        $dimension = $this->db->where('name', 'ukuran_kertas')->get('cms_settings')->row_array()['content'];
		    $dimension = (int) filter_var($dimension, FILTER_SANITIZE_NUMBER_INT);

        if ($dimension == 80){
          $strip = "------------------------------------------------\n";
        } else {
          $strip = "--------------------------------\n";
        }

        $logo = EscposImage::load('./assets/logo_black.png', false);
        $printerName = $this->db->where('name', 'printer_front')->get('cms_settings')->row_array();
        $ip = $this->input->ip_address();
        try {
          $profile = CapabilityProfile::load("simple");
          $connector = new WindowsPrintConnector("smb://Guest@" . $ip . '/' . $printerName['content']);
          // $connector = new WindowsPrintConnector($printerName['content']);
          // $connector = new WindowsPrintConnector('RODEOPRINTER');
          $printer = new Printer($connector);
          $printer->setJustification(Printer::JUSTIFY_CENTER);
          $printer->setTextSize(2, 2);
          $printer->text("RODEO CAR WASH");
          $printer->text("\n");
          $printer->setTextSize(2, 2);
          $printer->text($result->data->status);
          $printer->text("\n\n");

          $printer->qrCode($result->data->kode, Printer::QR_ECLEVEL_L, 11);
          $tanggal = $time = explode(" - ", $result->data->tanggal);
          $tanggal_masuk = $time = explode(" - ", $result->data->tanggal_masuk);

          $printer->text("\n\n");
          $printer->setTextSize(1, 1);
          $printer->text("--- " . $result->data->kode . " ---\n");
          $printer->text($tanggal[0] . "\n");
          $printer->text($this->myFunction("Jam Pesan", $tanggal[1]));
          $printer->text($this->myFunction("Jam Masuk", $tanggal_masuk[1]));
          $printer->text($this->myFunction("NOPOL", !empty($result->data->nomor_polisi) ? $result->data->nomor_polisi : "-"));
          $printer->text($this->myFunction("Kendaraan", $result->data->merek_kendaraan . " | " . $result->data->nama_kendaraan));
          $printer->text($strip);

          $printer->setJustification(Printer::JUSTIFY_LEFT);
          foreach ($result->data->detail_jasa as $value) {
              $printer->text($this->myFunction("1 x ", $value->nama_jasa));
          }
          $printer->feed(2);
          $printer->cut();
          $printer->close();
          echo "Print berhasil";
        } catch (Exception $e){
          echo "Print gagal";
        }
      }
    } else {
      echo "Internal Server Error";
    }
  }

  public function myFunction($name, $text){
    $url = new Save;
    $dimension = $this->db->where('name', 'ukuran_kertas')->get('cms_settings')->row_array()['content'];
		$dimension = (int) filter_var($dimension, FILTER_SANITIZE_NUMBER_INT);
		if($dimension == 80){
			$rightCols = 26; // 80 mm
    		$leftCols = 22; // 80 mm			
		} else{
			$rightCols = 22; // 58 mm
			$leftCols = 10; // 58 mm
		}
		
		$left = str_pad($name, $leftCols) ;
		$right = str_pad($text, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}