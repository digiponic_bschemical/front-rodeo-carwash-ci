<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  var $baseurl = 'http://localhost/rodeo-car-wash/public/api';

  public function __construct(){
    parent::__construct();
  }

	public function index(){
    $hours = $this->db->where('id', 1)->get('tb_cabang')->row();
    $carwash = $this->db->where('id_jenis_jasa', 6)->where('deleted_at', NULL)->get('tb_jasa')->result();
    $this->load->view('front', [
      'title'    => 'SILAHKAN PILIH TIPE KENDARAAN ANDA',
      'jamTutup' => $hours->jam_tutup,
      'carwash'  => array_chunk($carwash, 3),
      'scripts'  => ''
    ]);
  }
}
