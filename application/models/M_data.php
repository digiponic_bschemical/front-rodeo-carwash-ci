<?php
class M_data extends CI_Model{
  public function getId(){
    $now = date('Y-m-d');
    $id_booking = $this->db->where('DATE(created_at)', $now)->select_max('id', 'new_id')->get('tb_penjualan_jasa')->row_array();
    $id = $this->db->select_max('id', 'new_id')->get('tb_penjualan_jasa_detail')->row_array();
    return ['id_booking' => $id_booking['new_id'] + 1, 'id' => $id['new_id'] + 1];
  }
}
?>