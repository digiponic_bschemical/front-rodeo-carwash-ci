<?php defined('BASEPATH') OR exit('No direct script access allowed');
  if (!function_exists('curl')){
    function curl($url, $fields = NULL){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      if ($fields != NULL){
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      }
      $result = curl_exec($ch);
      if($result === FALSE){
        die('Curl failed: ' . curl_error($ch));
      }
      curl_close($ch);
      return $result;
    }
  }
?>